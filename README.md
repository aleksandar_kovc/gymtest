1. Clone repo
2. Run `composer install`
3. Setup your `.env` file (You can use configuration from `.env.example`, just set your db connection details)
4. Run `php artisan migrate --seed`
5. Run `php artisan serve`
6. Hit the endpoint `/api/reception?object_uuid=some_uuid&card_uuid=some_uuid`


I did not go inside too many details, validation messages are default ones, etc etc.
