<?php

namespace App\Exceptions;

use Exception;

class ReceptionException extends Exception
{
    /**
     * ReceptionException constructor.
     *
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, int $code)
    {
        parent::__construct($message, $code);
    }
}
