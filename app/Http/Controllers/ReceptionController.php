<?php

namespace App\Http\Controllers;

use App\Exceptions\ReceptionException;
use App\Http\Requests\ReceptionRequest;
use App\Http\Resources\ReceptionResource;
use App\Services\ReceptionService;
use Illuminate\Http\Response;

class ReceptionController extends Controller
{
    /**
     * Checks if user can enter the object.
     *
     * @param ReceptionRequest $request
     *
     * @return Response
     *
     * @throws ReceptionException
     */
    public function checkEntry(ReceptionRequest $request): Response
    {
        $entry = (new ReceptionService($request))->check();

        return response(new ReceptionResource($entry));
    }
}
