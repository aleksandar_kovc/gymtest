<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReceptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'status' => 'OK',
            'first_name' => $this->user->first_name,
            'last_name' => $this->user->last_name,
            'object_name' => $this->gym->object_name,
        ];
    }
}
