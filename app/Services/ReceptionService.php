<?php

namespace App\Services;

use App\Exceptions\ReceptionException;
use App\Models\Card;
use App\Models\Entry;
use App\Models\Gym;
use Illuminate\Http\Request;

class ReceptionService
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Gym
     */
    private $gym;

    /**
     * @var Card
     */
    private $card;

    /**
     * ReceptionService constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->gym = Gym::where('object_uuid', $request->object_uuid)->firstOrFail();
        $this->card = Card::where('card_uuid', $request->card_uuid)->firstOrFail();
    }

    /**
     * Checks if user can enter the object.
     *
     * @return Entry
     *
     * @throws ReceptionException
     */
    public function check(): Entry
    {
        $entryCheck = Entry::where([
            'gym_id' => $this->gym->id,
            'user_id' => $this->card->user_id,
            'date' => now()->toDateString(),
        ])->first();

        if ($entryCheck) {
            throw new ReceptionException('You already been here today.', 409);
        }

        return Entry::create([
            'gym_id' => $this->gym->id,
            'user_id' => $this->card->user_id,
            'date' => now()->toDateString(),
        ]);
    }
}
