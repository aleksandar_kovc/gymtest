<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        // I needed to remove dashes since alpha_num validation won't pass
        // so I'm not sure what is the good approach - to use real valid uuid, or to make sure that alpha_num validation works perfectly
        return [
            'card_uuid' => Str::replace('-', '', $this->faker->uuid()),
        ];
    }
}
