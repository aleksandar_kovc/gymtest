<?php

namespace Database\Seeders;

use App\Models\Card;
use App\Models\Gym;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        User::factory(10)->has(Card::factory()->count(1), 'cards')->create();
        Gym::factory(5)->create();
    }
}
